import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class ReadFile {

	public static void Read(Pokemon[] Pokemons) {
		
		int i=0;
		
		String PathArquivo = "data/csv_files/POKEMONS_DATA_1.csv";
		String PathArquivo2 = "data/csv_files/POKEMONS_DATA_2.csv";
		
		BufferedReader Conteudo = null;
		BufferedReader Conteudo2 = null;
		String Linha = "";
		String Linha2 = "";
		String Separador = ",";
		
		try {
			
			Conteudo = new BufferedReader(new FileReader(PathArquivo));
			Conteudo2 = new BufferedReader(new FileReader(PathArquivo2));
			
			Linha = Conteudo.readLine();
			Linha2 = Conteudo2.readLine();
			
			while( (Linha = Conteudo.readLine()) != null ){
				
				Linha2 = Conteudo2.readLine();
								
				String[] In = Linha.split(Separador);
				String[] In2 = Linha2.split(Separador);
				
				Pokemons[i] = new Pokemon();
					
				Pokemons[i].setId(In[0]);
				Pokemons[i].setName(In[1]);
				Pokemons[i].setType1(In[2]);
				Pokemons[i].setType2(In[3]);
				Pokemons[i].setTotal(In[4]);
				Pokemons[i].setHp(In[5]);
				Pokemons[i].setAttack(In[6]);
				Pokemons[i].setDefense(In[7]);
				Pokemons[i].setSpAtk(In[8]);
				Pokemons[i].setSpDef(In[9]);
				Pokemons[i].setSpeed(In[10]);
				Pokemons[i].setGeneration(In[11]);
				Pokemons[i].setLegendary(In[12]);
				Pokemons[i].setHeight(In2[3]);
				Pokemons[i].setWeight(In2[4]);
				Pokemons[i].setAbilitie_1(In2[5]);
				Pokemons[i].setAbilitie_2(In2[6]);
				Pokemons[i].setAbilitie_3(In2[7]);
				Pokemons[i].setMove_1(In2[8]);
				Pokemons[i].setMove_2(In2[9]);
				Pokemons[i].setMove_3(In2[10]);
				Pokemons[i].setMove_4(In2[11]);
				Pokemons[i].setMove_5(In2[12]);
				Pokemons[i].setMove_6(In2[13]);
				Pokemons[i].setMove_7(In2[14]);
				
				
				i++;
			}	
			
		} catch (FileNotFoundException e) {
			System.out.println("Arquivo não encontrado: "+e.getMessage());
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("IndexOutOfBounds: "+e.getMessage());
		} catch (IOException e) {
			System.out.println("Io error: "+e.getMessage());
		} finally {
			if(Conteudo != null) {
				try {
					
					Conteudo.close();
				} catch (IOException e) {
					System.out.println("IO Erro: "+e.getMessage());
				}
			}
			
		}
		
	}

}
