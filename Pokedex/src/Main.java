
public class Main {

	private static Treinador[] Treinadores = new Treinador[100];
	private static int QtdTreinadores = 0;

	private static Pokemon[] Pokemons = new Pokemon[722];

	public static void main(String[] args) {
		
		ReadFile.Read(Pokemons);
		
		Pokedex.main(args);
		
	}
	
	public static void AddTreinador(String Nome, int Idade, String Sexo) {
		
		Treinadores[QtdTreinadores]= new Treinador(Nome, Idade, Sexo);
	
		QtdTreinadores++;
	}
	
	public static void AddPokemonToTreinador(int Index, Pokemon pokemon) {
		Treinadores[Index].addPokemon(pokemon);
	}
	
	public static String NomeTreinadorAt(int Index) {
		return Treinadores[Index].getNome();
	}
	
	public static Treinador TreinadorAt(int Index) {
		
		return Treinadores[Index];
	}
	
	public static int sizeTreinadores() {
		return QtdTreinadores;
	}
	
	public static String NomePokemonAt(int Index) {
		return Pokemons[Index].getName();
	}
	
	public static String TipoPokemonAt(int Index) {
		return Pokemons[Index].getType1();
	}
	
	public static Pokemon PokemonAt(int Index) {
		return Pokemons[Index];
	}

}
