import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.CardLayout;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JList;
import javax.swing.ListSelectionModel;
import javax.swing.JScrollPane;
import javax.swing.ImageIcon;
import javax.swing.UIManager;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

public class Pokedex {

	private JFrame frame;
	private JPanel MenuPanel;
	private JPanel RegistrarPanel;
	private JPanel PokemonsPanel;
	private JPanel TreinadoresPanel;
	private JPanel MostrarPokemonPanel;
	private JTextField textFieldNome;
	private JTextField textFieldIdade;
	private JTextField txtPokemon;
	private JTextField txtTipo;
	private JLabel lblNewLabel_2;
	private JLabel Id;
	private JLabel Nome;
	private JLabel Hp;
	private JLabel Tipo;
	private JLabel Speed;
	private JLabel Attack;
	private JLabel Defence;
	private JLabel SpdAtk;
	private JLabel SpdDef;
	private JLabel Generation;
	private JLabel Height;
	private JLabel Weight;
	private JLabel Abilitie_1;
	private JLabel Abilitie_2;
	private JLabel Abilitie_3;
	private JLabel Move_1;
	private JLabel Move_2;
	private JLabel Move_3;
	private JLabel Move_4;
	private JLabel Move_5;
	private JLabel Move_6;
	private JLabel Move_7;
	private JLabel IdadeTreinador;
	private JLabel NomeTreinador;
	private JLabel SexoTreinador;
	private JLabel QtdPokemonsTreinador;
	private JLabel lblConfirmao;
	private JLabel lblConfirmaoIdade;
	private JLabel marcador;
	private JComboBox<String> CBSexo;
	private JPanel MostrarTreinadorPanel;
	private JPanel AdicionarPokemonPanel;
	private JList<String> listPokemonsTreindor;
	private JTextField textAddPokemon;
	
	/**
	 * Launch the application.
	 */
	
	
	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Pokedex window = new Pokedex();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	
	public Pokedex() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		frame = new JFrame();
		frame.setBounds(100, 100, 588, 418);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new CardLayout(0, 0));
		
		MenuPanel = new JPanel();
		frame.getContentPane().add(MenuPanel, "name_2360457094425");
		MenuPanel.setLayout(null);
		MenuPanel.setVisible(true);
		
		RegistrarPanel = new JPanel();
		frame.getContentPane().add(RegistrarPanel, "name_2360469178739");
		RegistrarPanel.setLayout(null);
		RegistrarPanel.setVisible(false);

		PokemonsPanel = new JPanel();
		frame.getContentPane().add(PokemonsPanel, "name_2360481374648");
		PokemonsPanel.setLayout(null);
		PokemonsPanel.setVisible(false);

		TreinadoresPanel = new JPanel();
		frame.getContentPane().add(TreinadoresPanel, "name_4014767523089");
		TreinadoresPanel.setLayout(null);
		TreinadoresPanel.setVisible(false);
		
		MostrarPokemonPanel = new JPanel();
		frame.getContentPane().add(MostrarPokemonPanel, "name_23645163483699");
		MostrarPokemonPanel.setLayout(null);
		
		JLabel label = new JLabel("Pokedex");
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setBounds(214, 124, 139, 64);
		label.setFont(new Font("Dialog", Font.BOLD, 24));
		MenuPanel.add(label);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(271, 55, 289, 304);
		PokemonsPanel.add(scrollPane);

		JList<String> listPokemons = new JList<String>();
		scrollPane.setViewportView(listPokemons);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(37, 73, 313, 280);
		TreinadoresPanel.add(scrollPane_1);

		JList<String> listTreinadores = new JList<String>();
		scrollPane_1.setViewportView(listTreinadores);
		listTreinadores.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		
		textFieldNome = new JTextField();
		textFieldNome.setBounds(150, 93, 287, 33);
		RegistrarPanel.add(textFieldNome);
		textFieldNome.setColumns(10);
		
		JLabel lblNome = new JLabel("Nome:");
		lblNome.setBounds(12, 93, 101, 33);
		lblNome.setFont(new Font("Dialog", Font.BOLD, 24));
		RegistrarPanel.add(lblNome);
		
		JLabel lblIdade = new JLabel("Idade:");
		lblIdade.setBounds(12, 171, 114, 25);
		lblIdade.setFont(new Font("Dialog", Font.BOLD, 24));
		RegistrarPanel.add(lblIdade);
		
		textFieldIdade = new JTextField();
		textFieldIdade.setBounds(150, 163, 114, 33);
		RegistrarPanel.add(textFieldIdade);
		textFieldIdade.setColumns(10);
		
		JLabel lblSexo = new JLabel("Sexo:");
		lblSexo.setBounds(12, 236, 114, 25);
		lblSexo.setFont(new Font("Dialog", Font.BOLD, 24));
		RegistrarPanel.add(lblSexo);
		
		JLabel lblNewLabel = new JLabel("Pokemon:");
		lblNewLabel.setBounds(22, 104, 117, 15);
		PokemonsPanel.add(lblNewLabel);
		
		
		txtPokemon = new JTextField();
		txtPokemon.setToolTipText("Pokemon");
		txtPokemon.setBounds(22, 136, 237, 39);
		PokemonsPanel.add(txtPokemon);
		txtPokemon.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Tipo:");
		lblNewLabel_1.setBounds(22, 199, 70, 15);
		PokemonsPanel.add(lblNewLabel_1);
		
		txtTipo = new JTextField();
		txtTipo.setToolTipText("Tipo");
		txtTipo.setBounds(22, 236, 237, 39);
		PokemonsPanel.add(txtTipo);
		txtTipo.setColumns(10);
		
		JLabel lblNomePokemon = new JLabel("Nome:");
		lblNomePokemon.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNomePokemon.setBounds(27, 83, 70, 15);
		MostrarPokemonPanel.add(lblNomePokemon);
		
		JLabel lblIdPokemon = new JLabel("Id:");
		lblIdPokemon.setHorizontalAlignment(SwingConstants.RIGHT);
		lblIdPokemon.setBounds(27, 56, 70, 15);
		MostrarPokemonPanel.add(lblIdPokemon);
		
		JLabel lblTipoPokemon = new JLabel("Tipo:");
		lblTipoPokemon.setHorizontalAlignment(SwingConstants.RIGHT);
		lblTipoPokemon.setBounds(27, 134, 70, 15);
		MostrarPokemonPanel.add(lblTipoPokemon);
		
		JLabel lblAttackPokemon = new JLabel("Attack:");
		lblAttackPokemon.setHorizontalAlignment(SwingConstants.RIGHT);
		lblAttackPokemon.setBounds(27, 161, 70, 15);
		MostrarPokemonPanel.add(lblAttackPokemon);
		
		JLabel lblDefencePokemon = new JLabel("Defence:");
		lblDefencePokemon.setHorizontalAlignment(SwingConstants.RIGHT);
		lblDefencePokemon.setBounds(27, 188, 70, 15);
		MostrarPokemonPanel.add(lblDefencePokemon);
		
		JLabel lblSpeed = new JLabel("Speed:");
		lblSpeed.setHorizontalAlignment(SwingConstants.RIGHT);
		lblSpeed.setBounds(27, 269, 70, 15);
		MostrarPokemonPanel.add(lblSpeed);
		
		JLabel lblSpdatk = new JLabel("SpdAtk:");
		lblSpdatk.setHorizontalAlignment(SwingConstants.RIGHT);
		lblSpdatk.setBounds(27, 215, 70, 15);
		MostrarPokemonPanel.add(lblSpdatk);
		
		JLabel lblSpddef = new JLabel("SpdDef:");
		lblSpddef.setHorizontalAlignment(SwingConstants.RIGHT);
		lblSpddef.setBounds(27, 242, 70, 15);
		MostrarPokemonPanel.add(lblSpddef);
		
		JLabel lblHp = new JLabel("Hp:");
		lblHp.setHorizontalAlignment(SwingConstants.RIGHT);
		lblHp.setBounds(27, 107, 70, 15);
		MostrarPokemonPanel.add(lblHp);
		
		JLabel lblGeneration = new JLabel("Generation:");
		lblGeneration.setHorizontalAlignment(SwingConstants.RIGHT);
		lblGeneration.setBounds(12, 293, 85, 15);
		MostrarPokemonPanel.add(lblGeneration);
		
		lblNewLabel_2 = new JLabel("");
		lblNewLabel_2.setBounds(474, 40, 102, 96);
		MostrarPokemonPanel.add(lblNewLabel_2);
		lblNewLabel_2.setIcon(new ImageIcon(Pokedex.class.getResource("/abra.png")));
		
		Id = new JLabel("Id");
		Id.setBackground(UIManager.getColor("Label.background"));
		Id.setBounds(109, 56, 132, 15);
		MostrarPokemonPanel.add(Id);
		
		Nome = new JLabel("Nome");
		Nome.setBounds(109, 83, 132, 15);
		MostrarPokemonPanel.add(Nome);
		
		Hp = new JLabel("Hp");
		Hp.setBounds(109, 107, 132, 15);
		MostrarPokemonPanel.add(Hp);
		
		Tipo = new JLabel("Tipo");
		Tipo.setBounds(109, 134, 132, 15);
		MostrarPokemonPanel.add(Tipo);
		
		Attack = new JLabel("Attack");
		Attack.setBounds(109, 161, 132, 15);
		MostrarPokemonPanel.add(Attack);
		
		Defence = new JLabel("Defence");
		Defence.setBounds(109, 188, 132, 15);
		MostrarPokemonPanel.add(Defence);
		
		SpdAtk = new JLabel("SpdAtk");
		SpdAtk.setBounds(109, 215, 132, 15);
		MostrarPokemonPanel.add(SpdAtk);
		
		SpdDef = new JLabel("SpdDef");
		SpdDef.setBounds(109, 242, 132, 15);
		MostrarPokemonPanel.add(SpdDef);
		
		Speed = new JLabel("Speed");
		Speed.setBounds(109, 269, 132, 15);
		MostrarPokemonPanel.add(Speed);
		
		Generation = new JLabel("Generation");
		Generation.setBounds(109, 293, 132, 15);
		MostrarPokemonPanel.add(Generation);
		
		JLabel lblHeight = new JLabel("Height:");
		lblHeight.setHorizontalAlignment(SwingConstants.RIGHT);
		lblHeight.setBounds(250, 56, 70, 15);
		MostrarPokemonPanel.add(lblHeight);
		
		JLabel lblWeight = new JLabel("Weight:");
		lblWeight.setHorizontalAlignment(SwingConstants.RIGHT);
		lblWeight.setBounds(250, 83, 70, 15);
		MostrarPokemonPanel.add(lblWeight);
		
		JLabel lblAbilitie = new JLabel("Abilitie 1:");
		lblAbilitie.setHorizontalAlignment(SwingConstants.RIGHT);
		lblAbilitie.setBounds(250, 107, 70, 15);
		MostrarPokemonPanel.add(lblAbilitie);
		
		JLabel lblAbilitie_1 = new JLabel("Abilitie 2:");
		lblAbilitie_1.setHorizontalAlignment(SwingConstants.RIGHT);
		lblAbilitie_1.setBounds(250, 134, 70, 15);
		MostrarPokemonPanel.add(lblAbilitie_1);
		
		JLabel lblAbilitie_2 = new JLabel("Abilitie 3:");
		lblAbilitie_2.setHorizontalAlignment(SwingConstants.RIGHT);
		lblAbilitie_2.setBounds(250, 161, 70, 15);
		MostrarPokemonPanel.add(lblAbilitie_2);
		
		JLabel lblMove = new JLabel("Move 1:");
		lblMove.setHorizontalAlignment(SwingConstants.RIGHT);
		lblMove.setBounds(250, 188, 70, 15);
		MostrarPokemonPanel.add(lblMove);
		
		JLabel lblMove_1 = new JLabel("Move 2:");
		lblMove_1.setHorizontalAlignment(SwingConstants.RIGHT);
		lblMove_1.setBounds(250, 215, 70, 15);
		MostrarPokemonPanel.add(lblMove_1);
		
		JLabel lblMove_2 = new JLabel("Move 3:");
		lblMove_2.setHorizontalAlignment(SwingConstants.RIGHT);
		lblMove_2.setBounds(250, 242, 70, 15);
		MostrarPokemonPanel.add(lblMove_2);
		
		JLabel lblMove_3 = new JLabel("Move 4:");
		lblMove_3.setHorizontalAlignment(SwingConstants.RIGHT);
		lblMove_3.setBounds(250, 269, 70, 15);
		MostrarPokemonPanel.add(lblMove_3);
		
		JLabel lblMove_4 = new JLabel("Move 5:");
		lblMove_4.setHorizontalAlignment(SwingConstants.RIGHT);
		lblMove_4.setBounds(250, 293, 70, 15);
		MostrarPokemonPanel.add(lblMove_4);
		
		JLabel lblMove_5 = new JLabel("Move 6:");
		lblMove_5.setHorizontalAlignment(SwingConstants.RIGHT);
		lblMove_5.setBounds(250, 320, 70, 15);
		MostrarPokemonPanel.add(lblMove_5);
		
		JLabel lblMove_6 = new JLabel("Move 7:");
		lblMove_6.setHorizontalAlignment(SwingConstants.RIGHT);
		lblMove_6.setBounds(250, 347, 70, 15);
		MostrarPokemonPanel.add(lblMove_6);
		
		Height = new JLabel("Height");
		Height.setBounds(332, 56, 130, 15);
		MostrarPokemonPanel.add(Height);
		
		Weight = new JLabel("Weight");
		Weight.setBounds(332, 83, 130, 15);
		MostrarPokemonPanel.add(Weight);
		
		Abilitie_1 = new JLabel("Abilitie 1");
		Abilitie_1.setBounds(332, 107, 130, 15);
		MostrarPokemonPanel.add(Abilitie_1);
		
		Abilitie_2 = new JLabel("Abilitie 2");
		Abilitie_2.setBounds(332, 134, 130, 15);
		MostrarPokemonPanel.add(Abilitie_2);
		
		Abilitie_3 = new JLabel("Abilitie 3");
		Abilitie_3.setBounds(332, 161, 130, 15);
		MostrarPokemonPanel.add(Abilitie_3);
		
		Move_1 = new JLabel("Move 1");
		Move_1.setBounds(332, 188, 130, 15);
		MostrarPokemonPanel.add(Move_1);
		
		Move_2 = new JLabel("Move 2");
		Move_2.setBounds(332, 215, 130, 15);
		MostrarPokemonPanel.add(Move_2);
		
		Move_3 = new JLabel("Move 3");
		Move_3.setBounds(332, 242, 130, 15);
		MostrarPokemonPanel.add(Move_3);
		
		Move_4 = new JLabel("Move 4");
		Move_4.setBounds(332, 269, 130, 15);
		MostrarPokemonPanel.add(Move_4);
		
		Move_5 = new JLabel("Move 5");
		Move_5.setBounds(332, 293, 130, 15);
		MostrarPokemonPanel.add(Move_5);
		
		Move_6 = new JLabel("Move 6");
		Move_6.setBounds(332, 320, 130, 15);
		MostrarPokemonPanel.add(Move_6);
		
		Move_7 = new JLabel("Move 7");
		Move_7.setBounds(332, 347, 130, 15);
		MostrarPokemonPanel.add(Move_7);
		
		MostrarTreinadorPanel = new JPanel();
		frame.getContentPane().add(MostrarTreinadorPanel, "name_4072110183254");
		MostrarTreinadorPanel.setLayout(null);
		
		
		JLabel lblNomeTreinador = new JLabel("Nome:");
		lblNomeTreinador.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNomeTreinador.setBounds(12, 91, 123, 15);
		MostrarTreinadorPanel.add(lblNomeTreinador);
		
		JLabel lblIdadeTreinador = new JLabel("Idade:");
		lblIdadeTreinador.setHorizontalAlignment(SwingConstants.RIGHT);
		lblIdadeTreinador.setBounds(12, 131, 123, 15);
		MostrarTreinadorPanel.add(lblIdadeTreinador);
		
		JLabel lblSexoTreinador = new JLabel("Sexo:");
		lblSexoTreinador.setHorizontalAlignment(SwingConstants.RIGHT);
		lblSexoTreinador.setBounds(12, 175, 123, 15);
		MostrarTreinadorPanel.add(lblSexoTreinador);
		
		JLabel lblQtdPokemonsTreinador = new JLabel("Qtd. Pokemons:");
		lblQtdPokemonsTreinador.setHorizontalAlignment(SwingConstants.RIGHT);
		lblQtdPokemonsTreinador.setBounds(12, 217, 123, 15);
		MostrarTreinadorPanel.add(lblQtdPokemonsTreinador);
		
		NomeTreinador = new JLabel("Nome");
		NomeTreinador.setBounds(147, 91, 70, 15);
		MostrarTreinadorPanel.add(NomeTreinador);
		
		IdadeTreinador = new JLabel("Idade");
		IdadeTreinador.setBounds(147, 131, 70, 15);
		MostrarTreinadorPanel.add(IdadeTreinador);
		
		SexoTreinador = new JLabel("Sexo");
		SexoTreinador.setBounds(147, 175, 70, 15);
		MostrarTreinadorPanel.add(SexoTreinador);
		
		QtdPokemonsTreinador = new JLabel("0");
		QtdPokemonsTreinador.setBounds(147, 217, 70, 15);
		MostrarTreinadorPanel.add(QtdPokemonsTreinador);
		
		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(351, 67, 199, 272);
		MostrarTreinadorPanel.add(scrollPane_2);
		
		listPokemonsTreindor = new JList<String>();
		scrollPane_2.setViewportView(listPokemonsTreindor);
		
		AdicionarPokemonPanel = new JPanel();
		frame.getContentPane().add(AdicionarPokemonPanel, "name_4701627376126");
		AdicionarPokemonPanel.setLayout(null);
		
		textAddPokemon = new JTextField();
		textAddPokemon.setBounds(48, 155, 452, 63);
		AdicionarPokemonPanel.add(textAddPokemon);
		textAddPokemon.setColumns(10);
		
		JLabel lblDigiteONome = new JLabel("Digite o Nome do Pokemon.");
		lblDigiteONome.setFont(new Font("Dialog", Font.BOLD, 20));
		lblDigiteONome.setBounds(48, 92, 335, 37);
		AdicionarPokemonPanel.add(lblDigiteONome);
		
		lblConfirmao = new JLabel("");
		lblConfirmao.setFont(new Font("Dialog", Font.PLAIN, 12));
		lblConfirmao.setBounds(58, 230, 182, 15);
		AdicionarPokemonPanel.add(lblConfirmao);
		
		CBSexo = new JComboBox<String>();
		CBSexo.setModel(new DefaultComboBoxModel<String>(new String[] {"Maculino", "Feminino", "Outros"}));
		CBSexo.setBounds(150, 236, 130, 28);
		RegistrarPanel.add(CBSexo);
		
		lblConfirmaoIdade = new JLabel("");
		lblConfirmaoIdade.setFont(new Font("Dialog", Font.PLAIN, 12));
		lblConfirmaoIdade.setBounds(298, 171, 143, 15);
		RegistrarPanel.add(lblConfirmaoIdade);
		
		marcador = new JLabel("");
		marcador.setHorizontalAlignment(SwingConstants.CENTER);
		marcador.setBounds(12, 306, 254, 70);
		marcador.setVisible(false);
		MostrarPokemonPanel.add(marcador);
		
		/*
		 * 
		 * 
		 * 
		 *  BOTOES
		 * 
		 * 
		 * 
		 */
		
		

		JButton btnRegistrar = new JButton("Registrar");
		btnRegistrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MenuPanel.setVisible(false);
				RegistrarPanel.setVisible(true);
				PokemonsPanel.setVisible(false);
				TreinadoresPanel.setVisible(false);
				MostrarPokemonPanel.setVisible(false);
				MostrarTreinadorPanel.setVisible(false);
				AdicionarPokemonPanel.setVisible(false);
				
			}
		});
		btnRegistrar.setBounds(223, 256, 120, 25);
		MenuPanel.add(btnRegistrar);
		
		JButton btnPoekmons = new JButton("Pokemons");
		btnPoekmons.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MenuPanel.setVisible(false);
				RegistrarPanel.setVisible(false);
				PokemonsPanel.setVisible(true);
				TreinadoresPanel.setVisible(false);
				MostrarPokemonPanel.setVisible(false);
				MostrarTreinadorPanel.setVisible(false);
				AdicionarPokemonPanel.setVisible(false);
			}
		});
		btnPoekmons.setBounds(223, 293, 120, 25);
		MenuPanel.add(btnPoekmons);
		
		JButton btnTreinadores = new JButton("Treinadores");
		btnTreinadores.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MenuPanel.setVisible(false);
				RegistrarPanel.setVisible(false);
				PokemonsPanel.setVisible(false);		
				TreinadoresPanel.setVisible(true);
				MostrarPokemonPanel.setVisible(false);
				MostrarTreinadorPanel.setVisible(false);
				AdicionarPokemonPanel.setVisible(false);
				
				Treinador aux;
				
				aux = Main.TreinadorAt(0);
				
				DefaultListModel<String> ListTrei = new DefaultListModel<String>();
				
				if(Main.sizeTreinadores() == 0) {
					
					
				}
				else {
					
					for(int j = 0; j < Main.sizeTreinadores(); j++) {
						
						
						aux = Main.TreinadorAt(j);
						ListTrei.addElement(aux.getNome());
						
					}
					
				}
				
				listTreinadores.setModel(ListTrei);
				
			}
		});
		btnTreinadores.setBounds(223, 219, 120, 25);
		MenuPanel.add(btnTreinadores);
		
		JButton btnVoltarRegistrar = new JButton("Voltar");
		btnVoltarRegistrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MenuPanel.setVisible(true);
				RegistrarPanel.setVisible(false);
				PokemonsPanel.setVisible(false);		
				TreinadoresPanel.setVisible(false);
				MostrarPokemonPanel.setVisible(false);
				MostrarTreinadorPanel.setVisible(false);
				AdicionarPokemonPanel.setVisible(false);
			}
		});
		btnVoltarRegistrar.setBounds(12, 12, 89, 25);
		RegistrarPanel.add(btnVoltarRegistrar);

		JButton btnNewButton = new JButton("Registrar");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String Nome;
				int Idade;
				String Sexo;
				
				try {
									
					Nome = textFieldNome.getText();
					Idade = Integer.parseInt(textFieldIdade.getText());
					Sexo = (String) CBSexo.getSelectedItem();
				
				}catch (NumberFormatException e1) {
					lblConfirmaoIdade.setText("Idade Inválida");
					textFieldIdade.setText("");
					return;
				}
					
				if(Idade < 0) {
					lblConfirmaoIdade.setText("Idade Inválida");
					return;
				}
				
				if(Main.sizeTreinadores() == 100) {
					lblConfirmaoIdade.setText("Limite de Treinadores");
					return;
				}
				
				Main.AddTreinador(Nome, Idade, Sexo);					
				textFieldNome.setText("");
				textFieldIdade.setText("");
				lblConfirmaoIdade.setText("");
					
			}
		});
		btnNewButton.setFont(new Font("Dialog", Font.BOLD, 20));
		btnNewButton.setBounds(207, 298, 151, 43);
		RegistrarPanel.add(btnNewButton);
		

		JButton btnVoltarPokemons = new JButton("Voltar");
		btnVoltarPokemons.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MenuPanel.setVisible(true);
				RegistrarPanel.setVisible(false);
				PokemonsPanel.setVisible(false);		
				TreinadoresPanel.setVisible(false);	
				MostrarPokemonPanel.setVisible(false);
				MostrarTreinadorPanel.setVisible(false);
				AdicionarPokemonPanel.setVisible(false);
			}
		});
		btnVoltarPokemons.setBounds(12, 12, 89, 25);
		PokemonsPanel.add(btnVoltarPokemons);

		JButton btnPesquisarPokemon = new JButton("Pesquisar");
		btnPesquisarPokemon.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String poke;
				
				poke = txtPokemon.getText();
				
				DefaultListModel<String> ListPoke = new DefaultListModel<String>();
				
				for(int j=0; j<722; j++) {
					if(poke.equalsIgnoreCase(Main.NomePokemonAt(j))) {
						ListPoke.addElement(Main.NomePokemonAt(j));
						listPokemons.setModel(ListPoke);
						return;
					}
				}
				
				ListPoke.addElement("Pokemon não encontrado");
				
				listPokemons.setModel(ListPoke);
				
			}
		});
		btnPesquisarPokemon.setBounds(142, 99, 117, 25);
		PokemonsPanel.add(btnPesquisarPokemon);
		
		JButton btnPesquisarTipo = new JButton("Pesquisar");
		btnPesquisarTipo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String tipo;
				boolean Nenhum = true;
				
				tipo = txtTipo.getText();
				
				DefaultListModel<String> ListPoke = new DefaultListModel<String>();
				
				for(int j=0; j<722; j++) {
					if(tipo.equalsIgnoreCase(Main.TipoPokemonAt(j))) {
						ListPoke.addElement(Main.NomePokemonAt(j));
						Nenhum = false;
					}
				}
				
				if(Nenhum) {					
					ListPoke.addElement("Pokemon não encontrado");
				}
				
				listPokemons.setModel(ListPoke);
				
			}
		});
		btnPesquisarTipo.setBounds(142, 199, 117, 25);
		PokemonsPanel.add(btnPesquisarTipo);
		
		JButton btnMostrarPokemon = new JButton("Mostrar Pokemon");
		btnMostrarPokemon.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				int iSelected;
				String itemName;
				String pathImage;
				Pokemon aux = new Pokemon();
				
				
				iSelected = listPokemons.getSelectedIndex();
				itemName = listPokemons.getSelectedValue();
				
				if(iSelected == -1) {
					return;
				}
				
				if(itemName.equals("Pokemon não encontrado")) {
					return;
				}
				
					
				MenuPanel.setVisible(false);
				RegistrarPanel.setVisible(false);
				PokemonsPanel.setVisible(false);		
				TreinadoresPanel.setVisible(false);	
				MostrarPokemonPanel.setVisible(true);
				MostrarTreinadorPanel.setVisible(false);
				AdicionarPokemonPanel.setVisible(false);
				
				if(!itemName.equals("Julio")) {
					
				pathImage = "/" + itemName.toLowerCase() + ".png";
				lblNewLabel_2.setIcon(new ImageIcon(Pokedex.class.getResource(pathImage)));
				
				}
				
				for(int j=0; j<722; j++) {
					if(itemName.equals(Main.NomePokemonAt(j))) {
						aux = Main.PokemonAt(j);
					}
				}
				
				
				Id.setText(aux.getId());
				Nome.setText(aux.getName());
				Hp.setText(aux.getHp());
				Speed.setText(aux.getSpeed());
				Tipo.setText(aux.getType1());
				Attack.setText(aux.getAttack());
				Defence.setText(aux.getDefense());
				SpdAtk.setText(aux.getSpAtk());
				SpdDef.setText(aux.getSpDef());
				Generation.setText(aux.getGeneration());
				Height.setText(aux.getHeight());
				Weight.setText(aux.getWeight());
				Abilitie_1.setText(aux.getAbilitie_1());
				Abilitie_2.setText(aux.getAbilitie_2());
				Abilitie_3.setText(aux.getAbilitie_3());
				Move_1.setText(aux.getMove_1());
				Move_2.setText(aux.getMove_2());
				Move_3.setText(aux.getMove_3());
				Move_4.setText(aux.getMove_4());
				Move_5.setText(aux.getMove_5());
				Move_6.setText(aux.getMove_6());
				Move_7.setText(aux.getMove_7());
				
				marcador.setText("2");
				
				if(itemName.equals("Julio")) {
					marcador.setText("Pokemon raro nunca visto");
					marcador.setVisible(true);
					lblNewLabel_2.setVisible(false);
				}
				
			}
		});
		btnMostrarPokemon.setBounds(22, 287, 237, 72);
		PokemonsPanel.add(btnMostrarPokemon);
		
		
		JButton btnVoltarTreinador = new JButton("Voltar");
		btnVoltarTreinador.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MenuPanel.setVisible(true);
				RegistrarPanel.setVisible(false);
				PokemonsPanel.setVisible(false);		
				TreinadoresPanel.setVisible(false);
				MostrarPokemonPanel.setVisible(false);
				MostrarTreinadorPanel.setVisible(false);
				AdicionarPokemonPanel.setVisible(false);
			}
		});
		btnVoltarTreinador.setBounds(12, 12, 89, 25);
		TreinadoresPanel.add(btnVoltarTreinador);
		
		JButton btnMostrarTreinador = new JButton("Mostrar Treinador");
		btnMostrarTreinador.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				int iSelected;
				Treinador aux;
				
				iSelected = listTreinadores.getSelectedIndex();
				
				if(iSelected == -1) {
					return;
				}
				
				MenuPanel.setVisible(false);
				RegistrarPanel.setVisible(false);
				PokemonsPanel.setVisible(false);		
				TreinadoresPanel.setVisible(false);	
				MostrarPokemonPanel.setVisible(false);
				MostrarTreinadorPanel.setVisible(true);
				AdicionarPokemonPanel.setVisible(false);
				
				aux = Main.TreinadorAt(iSelected);
				
				NomeTreinador.setText(aux.getNome());
				IdadeTreinador.setText(Integer.toString(aux.getIdade()));
				SexoTreinador.setText(aux.getSexo());
				QtdPokemonsTreinador.setText(Integer.toString(aux.getQtdPokemons()));
				
				
				DefaultListModel<String> ListPokeTreinador = new DefaultListModel<String>();

				if(aux.getQtdPokemons() == 0) {
					ListPokeTreinador.addElement("Nenhum Pokemon");
					//listPokemonsTreindor.
				}
							
				for(int j=0; j<aux.getQtdPokemons(); j++) {

						ListPokeTreinador.addElement(aux.PokemonAt(j));
					
				}
					
					
					listPokemonsTreindor.setModel(ListPokeTreinador);
				
			}
		});
		btnMostrarTreinador.setBounds(370, 73, 206, 60);
		TreinadoresPanel.add(btnMostrarTreinador);
		
		JButton btnAdicionarPokemon = new JButton("Adicionar Pokemon");
		btnAdicionarPokemon.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if(listTreinadores.getSelectedIndex() != -1) {
					
					MenuPanel.setVisible(false);
					RegistrarPanel.setVisible(false);
					PokemonsPanel.setVisible(false);		
					TreinadoresPanel.setVisible(false);	
					MostrarPokemonPanel.setVisible(false);
					MostrarTreinadorPanel.setVisible(false);
					AdicionarPokemonPanel.setVisible(true);
					
				}
				
			}
		});
		btnAdicionarPokemon.setBounds(370, 145, 206, 60);
		TreinadoresPanel.add(btnAdicionarPokemon);
		
		JButton btnVoltarMostrar = new JButton("Voltar");
		btnVoltarMostrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String aux;
				
				aux = marcador.getText();
				
				if(aux.equals("1")) {
					
					MenuPanel.setVisible(false);
					RegistrarPanel.setVisible(false);
					PokemonsPanel.setVisible(false);		
					TreinadoresPanel.setVisible(false);	
					MostrarPokemonPanel.setVisible(false);
					MostrarTreinadorPanel.setVisible(true);
					AdicionarPokemonPanel.setVisible(false);
					
				}else {
					
					MenuPanel.setVisible(false);
					RegistrarPanel.setVisible(false);
					PokemonsPanel.setVisible(true);		
					TreinadoresPanel.setVisible(false);	
					MostrarPokemonPanel.setVisible(false);
					MostrarTreinadorPanel.setVisible(false);
					AdicionarPokemonPanel.setVisible(false);
					
				}
				
				marcador.setVisible(false);
				lblNewLabel_2.setVisible(true);
				
			}
		});
		btnVoltarMostrar.setBounds(12, 12, 117, 25);
		MostrarPokemonPanel.add(btnVoltarMostrar);
		

		JButton btnVoltarMostrarTreinador = new JButton("Voltar");
		btnVoltarMostrarTreinador.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MenuPanel.setVisible(false);
				RegistrarPanel.setVisible(false);
				PokemonsPanel.setVisible(false);		
				TreinadoresPanel.setVisible(true);	
				MostrarPokemonPanel.setVisible(false);
				MostrarTreinadorPanel.setVisible(false);
				AdicionarPokemonPanel.setVisible(false);
			}
		});
		btnVoltarMostrarTreinador.setBounds(12, 12, 117, 25);
		MostrarTreinadorPanel.add(btnVoltarMostrarTreinador);
		
		JButton btnMostrarPokeTreinador = new JButton("Mostrar Pokemon");
		btnMostrarPokeTreinador.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				int iSelected;
				String itemName;
				String pathImage;
				Pokemon aux = new Pokemon();
				
				
				iSelected = listPokemonsTreindor.getSelectedIndex();
				itemName = listPokemonsTreindor.getSelectedValue();
				
				if(iSelected == -1) {
					return;
				}
				
				if(itemName.equals("Pokemon não encontrado")) {
					return;
				}
				
				MenuPanel.setVisible(false);
				RegistrarPanel.setVisible(false);
				PokemonsPanel.setVisible(false);		
				TreinadoresPanel.setVisible(false);	
				MostrarPokemonPanel.setVisible(true);
				MostrarTreinadorPanel.setVisible(false);
				AdicionarPokemonPanel.setVisible(false);
					
					
				pathImage = "/" + itemName.toLowerCase() + ".png";
				
				lblNewLabel_2.setIcon(new ImageIcon(Pokedex.class.getResource(pathImage)));
				
				for(int j=0; j<720; j++) {
					if(itemName.equals(Main.NomePokemonAt(j))) {
						aux = Main.PokemonAt(j);
					}
				}
				
				
				Id.setText(aux.getId());
				Nome.setText(aux.getName());
				Hp.setText(aux.getHp());
				Speed.setText(aux.getSpeed());
				Tipo.setText(aux.getType1());
				Attack.setText(aux.getAttack());
				Defence.setText(aux.getDefense());
				SpdAtk.setText(aux.getSpAtk());
				SpdDef.setText(aux.getSpDef());
				Generation.setText(aux.getGeneration());
				Height.setText(aux.getHeight());
				Weight.setText(aux.getWeight());
				Abilitie_1.setText(aux.getAbilitie_1());
				Abilitie_2.setText(aux.getAbilitie_2());
				Abilitie_3.setText(aux.getAbilitie_3());
				Move_1.setText(aux.getMove_1());
				Move_2.setText(aux.getMove_2());
				Move_3.setText(aux.getMove_3());
				Move_4.setText(aux.getMove_4());
				Move_5.setText(aux.getMove_5());
				Move_6.setText(aux.getMove_6());
				Move_7.setText(aux.getMove_7());
				
				marcador.setText("1");
				
			}
		});
		btnMostrarPokeTreinador.setBounds(96, 272, 168, 68);
		MostrarTreinadorPanel.add(btnMostrarPokeTreinador);

		JButton btnVoltarAdicionar = new JButton("Voltar");
		btnVoltarAdicionar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				MenuPanel.setVisible(false);
				RegistrarPanel.setVisible(false);
				PokemonsPanel.setVisible(false);		
				TreinadoresPanel.setVisible(true);	
				MostrarPokemonPanel.setVisible(false);
				MostrarTreinadorPanel.setVisible(false);
				AdicionarPokemonPanel.setVisible(false);
				
				lblConfirmao.setText("");
			}
		});
		btnVoltarAdicionar.setBounds(12, 12, 117, 25);
		AdicionarPokemonPanel.add(btnVoltarAdicionar);
		
		JButton btnAdicionar = new JButton("Adicionar");
		btnAdicionar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String nome;
				Pokemon aux = null;
				int iSelected;
				boolean achou = false;
				
				iSelected = listTreinadores.getSelectedIndex();
				
				nome = textAddPokemon.getText();
				
				for(int j=0; j<721; j++) {
					if(nome.equalsIgnoreCase(Main.NomePokemonAt(j))){
						aux  = Main.PokemonAt(j);
						achou = true;
					}
				}
				
				if(!achou) {
					lblConfirmao.setText("Pokemon não encontrado");
				}else {
					
				
				Main.AddPokemonToTreinador(iSelected, aux);
				
				textAddPokemon.setText("");
				
				lblConfirmao.setText("Pokemon Adicionado");
				}
				
			}
		});
		btnAdicionar.setBounds(205, 290, 145, 54);
		AdicionarPokemonPanel.add(btnAdicionar);
		
		
	}
}
