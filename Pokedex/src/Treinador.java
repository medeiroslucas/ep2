
public class Treinador extends Pessoa{
	
	private Pokemon[] Pokemons = new Pokemon[1000];
	private int QtdPokemons = 0;
	

	public void addPokemon(Pokemon pokemon) {
		Pokemons[QtdPokemons] = pokemon;
		QtdPokemons++;
	}
	
	public int getQtdPokemons() {
		return QtdPokemons;
	}
	
	public String PokemonAt(int Index) {
		return Pokemons[Index].getName();
	}
	
	public Treinador(String Nome, int Idade, String Sexo) {
		
		setNome(Nome);
		setIdade(Idade);
		setSexo(Sexo);
		
	}
	
	public Treinador() {
	
		setNome("");
		setIdade(0);
		setSexo("");
	}

	
}
