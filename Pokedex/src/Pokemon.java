
public class Pokemon {

	private String Id;
	private String Total;
	private String Hp;
	private String Attack;
	private String Defense;
	private String SpAtk;
	private String SpDef;
	private String Speed;
	private String Generation;
	private String Name;
	private String Type1;
	private String Type2;
	private String Legendary;
	private String Height;
	private String Weight;
	private String Abilitie_1;
	private String Abilitie_2;
	private String Abilitie_3;
	private String Move_1;
	private String Move_2;
	private String Move_3;
	private String Move_4;
	private String Move_5;
	private String Move_6;
	private String Move_7;
	
	

	
	//Height,Weight,Abilitie 1,Abilitie 2,Abilitie 3,Move 1,Move 2,Move 3,Move 3,Move 5,Move 6,Move 7
	
	public String getId() {
		return Id;
	}
	public void setId(String id) {
		Id = id;
	}
	public String getTotal() {
		return Total;
	}
	public void setTotal(String total) {
		Total = total;
	}
	public String getHp() {
		return Hp;
	}
	public void setHp(String hp) {
		Hp = hp;
	}
	public String getAttack() {
		return Attack;
	}
	public void setAttack(String attack) {
		Attack = attack;
	}
	public String getDefense() {
		return Defense;
	}
	public void setDefense(String defense) {
		Defense = defense;
	}
	public String getSpAtk() {
		return SpAtk;
	}
	public void setSpAtk(String spAtk) {
		SpAtk = spAtk;
	}
	public String getSpDef() {
		return SpDef;
	}
	public void setSpDef(String spDef) {
		SpDef = spDef;
	}
	public String getSpeed() {
		return Speed;
	}
	public void setSpeed(String speed) {
		Speed = speed;
	}
	public String getGeneration() {
		return Generation;
	}
	public void setGeneration(String generation) {
		Generation = generation;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getType1() {
		return Type1;
	}
	public void setType1(String type1) {
		Type1 = type1;
	}
	public String getType2() {
		return Type2;
	}
	public void setType2(String type2) {
		Type2 = type2;
	}
	public String getLegendary() {
		return Legendary;
	}
	public void setLegendary(String legendary) {
		Legendary = legendary;
	}
	
	public void printPokemon() {
		System.out.println(Id);
		System.out.println(Name);
	}

	public String getHeight() {
		return Height;
	}
	public void setHeight(String height) {
		Height = height;
	}
	public String getWeight() {
		return Weight;
	}
	public void setWeight(String weight) {
		Weight = weight;
	}
	public String getAbilitie_1() {
		return Abilitie_1;
	}
	public void setAbilitie_1(String abilitie_1) {
		Abilitie_1 = abilitie_1;
	}
	public String getAbilitie_2() {
		return Abilitie_2;
	}
	public void setAbilitie_2(String abilitie_2) {
		Abilitie_2 = abilitie_2;
	}
	public String getAbilitie_3() {
		return Abilitie_3;
	}
	public void setAbilitie_3(String abilitie_3) {
		Abilitie_3 = abilitie_3;
	}
	public String getMove_1() {
		return Move_1;
	}
	public void setMove_1(String move_1) {
		Move_1 = move_1;
	}
	public String getMove_2() {
		return Move_2;
	}
	public void setMove_2(String move_2) {
		Move_2 = move_2;
	}
	public String getMove_3() {
		return Move_3;
	}
	public void setMove_3(String move_3) {
		Move_3 = move_3;
	}
	public String getMove_4() {
		return Move_4;
	}
	public void setMove_4(String move_4) {
		Move_4 = move_4;
	}
	public String getMove_5() {
		return Move_5;
	}
	public void setMove_5(String move_5) {
		Move_5 = move_5;
	}
	public String getMove_6() {
		return Move_6;
	}
	public void setMove_6(String move_6) {
		Move_6 = move_6;
	}
	public String getMove_7() {
		return Move_7;
	}
	public void setMove_7(String move_7) {
		Move_7 = move_7;
	}
	
	
}